#!python3

import json
import re

import ccxt
import PySimpleGUI as sg
from schedule import Job, Scheduler


class SafeScheduler(Scheduler):
    """
    An implementation of Scheduler that catches jobs that fail, logs their
    exception tracebacks as errors, and keeps going.
    Use this to run jobs that may or may not crash without worrying about
    whether other jobs will run or if they'll crash the entire script.
    """

    def __init__(self, rerun_immediately=True):
        self.rerun_immediately = rerun_immediately
        super().__init__()

    def _run_job(self, job: Job):
        try:
            super()._run_job(job)
        except Exception:  # pylint: disable=broad-except
            if not self.rerun_immediately:
                # Reschedule the job for the next time it was meant to run, instead of
                # letting it run
                # next tick
                job._schedule_next_run()  # pylint: disable=protected-access


# init values
binance_api_key = ""
binance_secret_key = ""
kraken_api_key = ""
kraken_secret_key = ""
supported_coins = []

schedule = SafeScheduler()

# read configs
with open('config.json') as f:
    data = json.load(f)
    # binance
    binance_api_key = data["binance"]["binance_api_key"]
    binance_secret_key = data["binance"]["binance_secret_key"]
    # kraken
    kraken_api_key = data["kraken"]["kraken_api_key"]
    kraken_secret_key = data["kraken"]["kraken_secret_key"]
    # supported_coins
    supported_coins = data["supported_coins"]
    # Check_balance_sleep_time
    SCHEDULER_SECs = data["Check_balance_sleep_time"]

binance_exchange = ccxt.binance({'apiKey':binance_api_key ,'secret': binance_secret_key,'enableRateLimit': True, })
kraken_exchange = ccxt.kraken({'apiKey': kraken_api_key,'secret': kraken_secret_key,'enableRateLimit': True, })

def get_ticker_price(exchange, ticker):
    price = 65987
    # TODO
    return "65987"


def make_sell_limit_order():
    pass


def is_sell_order_executed():
    pass


def make_buy_limit_order():
    pass


# run scheduled job
schedule.every(SCHEDULER_SECs).seconds.do(
    is_sell_order_executed).tag("checkSellLimitOrder")

sg.theme('Topanga')
# All the stuff inside your window.
layout = [
    [sg.Text('Exchange :'), sg.Combo(values=sorted(['binance', 'kraken']), default_value='binance', enable_events=True,
                                     key='_EXCHANGE_', visible=True, readonly=True, tooltip='From Currency', font='Courier 12', )],
    [sg.Text('From :'), sg.Combo(values=sorted(supported_coins), default_value='BTC', enable_events=True, key='_FROM_CURRENCY_', visible=True, readonly=True, tooltip='From Currency', disabled=False, font='Courier 12'), sg.Text(
        'To :'), sg.Combo(values=sorted(supported_coins), default_value='USDT', enable_events=True, key='_TO_CURRENCY_', visible=True, readonly=True, tooltip='TO Currency', disabled=False, font='Courier 12', size=(10, 1))],
    [sg.Text('Current Price      :'), sg.Text('', key='_CURRNET_PRICE_')],
    [sg.HorizontalSeparator()],
    [sg.Text('Selling Price    :'), sg.InputText(
        key='_SELL_PRICE_', focus=True, font='Courier 12', size=(15, 1))],
    [sg.Text('Quantity to sell :'), sg.InputText(
        key='_QTY_To_SELL_', font='Courier 12', size=(15, 1))],
    [sg.Text('Buying Price     :'), sg.InputText(
        key='_BUY_PRICE_', font='Courier 12', size=(15, 1))],
    [sg.HorizontalSeparator()],
    [sg.Button('Ok')]
]


# Create the Window
window = sg.Window('Krypto Seller & Buyer', layout)
# Event Loop to process "events" and get the "values" of the inputs
while True:
    schedule.run_pending()
    event, values = window.read()
    if event == sg.WIN_CLOSED:
        break
    if event == 'Ok':
        # Check is ticker supported by the exchange   
        try:
            if "binance" == values["_EXCHANGE_"]:
                window['_CURRNET_PRICE_'].update(binance_exchange.fetch_ticker(
                    values["_FROM_CURRENCY_"]+"/"+values["_TO_CURRENCY_"])['last'])
            elif "kraken" == values["_EXCHANGE_"]:
                window['_CURRNET_PRICE_'].update(kraken_exchange.fetch_ticker(
                    values["_FROM_CURRENCY_"]+"/"+values["_TO_CURRENCY_"])['last'])
            # Check if given prices are floats
            if re.match('^\d*\.?\d+$', values['_SELL_PRICE_']) and re.match('^\d*\.?\d+$', values['_BUY_PRICE_']) and re.match('^\d*\.?\d+$', values['_QTY_To_SELL_']):
                # Check if Sell value is bigger than buy value
                if values['_BUY_PRICE_'] >= values['_SELL_PRICE_']:
                    sg.popup_error('BUY Price must be less than SELL price', title='Error!')
                elif sg.popup_yes_no('Do you really want to make BUY/SELL orders?', title = 'Confirm Sell!') == 'Yes':
                    print(
                        f"You want to sell {values['_FROM_CURRENCY_']} and buy {values['_TO_CURRENCY_']} when the price equal to {values['_SELL_PRICE_']}")
            else:
                sg.popup_error('Try to enter a numeric value!', title='Error!')
        except Exception as e:
            sg.popup_error(e, title='Error!')

window.close()
