#!python3
# Implemented By: @b3ljTM

import re
from os import path

import PySimpleGUI as sg

from config import Config
from database import Database
from exchange_api_manager import ExchangeAPIManager
from logger import Logger
from scheduler import SafeScheduler

config = Config()
logger = Logger()
db = Database(logger, config)
schedule = SafeScheduler(logger)

binance_manager = ExchangeAPIManager("binance", config, db, logger)
coinbase_manager = ExchangeAPIManager("coinbase", config, db, logger)
kraken_manager = ExchangeAPIManager("kraken", config, db, logger)

def get_current_price(exchange='binance',ticker='BTC/USDT'):
    try : 
        if "binance" == exchange:
                return (binance_manager.get_price(ticker))
        elif "coinbase" == exchange:
                return (coinbase_manager.get_price(ticker))
        elif "kraken" == exchange:
            return (kraken_manager.get_price(ticker))
        else:
            pass
    except Exception as e:
        return -1, e

def get_balance(exchange='binance',currency='BTC'):
    try : 
        if "binance" == exchange:
                return (binance_manager.get_balance(currency))
        elif "coinbase" == exchange:
                return (coinbase_manager.get_balance(currency))
        elif "kraken" == exchange:
            return (kraken_manager.get_balance(currency))
        else:
            pass
    except Exception as e:
        return -1, e

def make_sell_order(exchange, symbol, amount, price):
    try : 
        if "binance" == exchange:
                return (binance_manager.set_sell_order(symbol, amount, price))
        elif "coinbase" == exchange:
                return (coinbase_manager.set_sell_order(symbol, amount, price))
        elif "kraken" == exchange:
            return (kraken_manager.set_sell_order(symbol, amount, price))
        else:
            pass
    except Exception as e:
        return -1, e

def is_float(value):
    if re.match('^\d*\.?\d+$', value):
        return True
    else:
        return False

def main():
    sg.theme('Topanga')

    frame_parameters = [
        [sg.Text('Sell price:', size=(15, 1)), sg.Input( key='_SELL_PRICE_', size=(19, 1))],
        [sg.Text('Rebuy price :', size=(15, 1)), sg.Input(key='_REBUY_PRICE_', size=(19, 1))],
        [sg.Text('How much to rebuy?: '), sg.Radio('All in', 'radio_rebuy', default=True, key='_ALL_IN_'), sg.Radio('Same amount', 'radio_rebuy', key='_SOME_SOLD_',  size=(10, 1))],
        [sg.Text('Selling quantity or percentage ? : '), sg.Radio('Qty', 'radio_qty_per', default=True, key='_QTY_'), sg.Radio('%', 'radio_qty_per', key='_PER_', size=(10, 1))],
        [sg.Text('amount to SELL:', size=(15, 1)), sg.Input(key='_AMOUNT_TO_SELL_', size=(19, 1))],
        [sg.Checkbox('Using Stop Loss?: ', key='_USE_STOP_LOSS_', default=False, enable_events=True, size=(15, 1))],
        [sg.Text('Stop Loss value:', size=(15, 1)), sg.Input(key='_STOP_LOSS_VALUE_', disabled=True, size=(19, 1))],
        [sg.HorizontalSeparator(color="darkred", pad=(30, 5))],
        [sg.Text('Expected Fees:', size=(15, 1)), sg.Text('0.000', key='_EXPECTED_FEES_', size=(19, 1))],
        [sg.Text('Expacted Gain:', size=(15, 1)), sg.Text('0.000', key='_EXPECTED_GAIN_', size=(19, 1),)],
        [sg.Text('Expacted Loss:', size=(15, 1)), sg.Text('0.000', key='_EXPECTED_LOSS_', size=(19, 1),)],
        [sg.Button('Confirm', key='_Confirm_Button_')],
    ]
    frame_userInfo = [
        [sg.Text('Exchange :', size=(15, 1)), sg.Combo(values=sorted(['binance', 'coinbase', 'kraken']), default_value='binance', enable_events=True, key='_EXCHANGE_', visible=True, readonly=True, tooltip='From Currency', font='Courier 12', )],
                    [sg.Text('Ticker :', size=(15, 1)), sg.Combo(values=sorted(config.supported_tickers), default_value='BTC/USDT', enable_events=True,key='_TICKER_', visible=True, readonly=True, tooltip='Ticker to trade', disabled=False, font='Courier 12'), sg.Text('Current price:', size=(12, 1)), sg.Text('0.000', key='_CURRENT_PRICE_',background_color='gray4', font=('Default', 13), size=(12, 1)), sg.Button('Update')],
                    [sg.Text('Balance:', size=(15, 1)), sg.Text('0.000',key="_BALANCE_", size=(15, 1))],
                    [sg.Text('Free Balance:', size=(15, 1)), sg.Text('0.000', key="_FREE_BALANCE_", background_color='gray4', font=('Default', 13), size=(15, 1))],
                    [sg.Text('Locked Balance:', size=(15, 1)), sg.Text('0.000', key='_LOCKED_BALANCE_', size=(15, 1))],
    ]
    frame_orders = [
        [sg.Table(values=[["BTC/USDT", "1", "sell", "3", "65000", "Cancel"], ["BTC/USDT", "2", "buy", "3", "65000"], ["BTC/USDT", "3", "sell", "3", "65000"], ["BTC/USDT", "4", "sell", "3", "65000"]],
                            headings=["ticker", "orderId", "side", "amount", "price", "status"], max_col_width=25,  auto_size_columns=True, justification='c', alternating_row_color='gray4',size=(550,500))]
    ]
    #The final layout is a simple one
    layout = [
        [sg.Frame('Info:', frame_userInfo, font=( 'Default', 12), pad=(3, 3), size=(600, 160))],
        [sg.Frame('Actions:', frame_parameters, font=('Default', 12), pad=(3, 3), size=(600, 330))],
        [sg.Frame('Orders:', frame_orders, font=('Default', 12), pad=(3, 3), size=(600, 300))],
    ]

    # layout = [
    #             [
    #                 sg.Column(
    #                             [
    #                                 [sg.Column([sg.Frame('Info: ', frame_userInfo, font=( 'Default', 12), pad=(3, 3), size=(600, 155))])],
    #                                 [sg.Column([sg.Frame('Acrions: ', frame_parameters, font=('Default', 12), pad=(3, 3), size=(600, 305))])],
    #                             ]
    #                         ),
    #                 sg.Column([sg.Frame('Orders: ', frame_orders, font=('Default', 12), pad=(3, 3), size=(600, 300))])
    #            ]
    #         ]

    window = sg.Window('Krypto Seller & Rebuyer', layout, resizable=True)

    # ----------------  main loop  ----------------
    while True:
        # --------- Read and update window --------
        event, values = window.read()
        balance = get_balance(exchange=values['_EXCHANGE_'] ,currency=values['_TICKER_'].split("/")[0])
        #update current price
        window['_CURRENT_PRICE_'].update(str(get_current_price(exchange=values['_EXCHANGE_'] ,ticker=values['_TICKER_'])))
        #update totla balance
        window['_BALANCE_'].update(str(balance['total']))
        #update free balance
        window['_FREE_BALANCE_'].update(str(balance['free']))
        #update locked balance
        window['_LOCKED_BALANCE_'].update(str(balance['used']))
        #update stop loss field
        window['_STOP_LOSS_VALUE_'].update(disabled = not values['_USE_STOP_LOSS_'])

        if event == sg.WIN_CLOSED:
            break
        # --------- Do Confirmation Button Operations --------
        elif event == '_Confirm_Button_':
            #Check that values are numeric
            if not is_float(values['_SELL_PRICE_']):
                sg.popup_error('Sell price must be numeric!', title='Error!'); continue
            if not is_float(values['_REBUY_PRICE_']):
                sg.popup_error('Rebuy price must be numeric!', title='Error!'); continue
            #Check if Sell price is greater than rebuy price
            elif values['_REBUY_PRICE_'] >= values['_SELL_PRICE_']:
                sg.popup_error('BUY Price must be less than SELL price', title='Error!'); continue
            if not is_float(values['_AMOUNT_TO_SELL_']):
                sg.popup_error('Amount to sell  must be numeric!', title='Error!'); continue
            if not is_float(values['_STOP_LOSS_VALUE_']) and values['_USE_STOP_LOSS_']:
                sg.popup_error('Stop loss must be numeric!', title='Error!'); continue
            elif values['_STOP_LOSS_VALUE_'] <= values['_SELL_PRICE_']:
                sg.popup_error('Stop loss must be greater than sell price', title='Error!'); continue 
            #Confirm choices
            if sg.popup_yes_no(f"Do you want to confirm orders?\n sell price: {values['_SELL_PRICE_']} \n amount to sell: {values['_AMOUNT_TO_SELL_']}\n Rebuy price: {values['_REBUY_PRICE_']} \n Stop Loss: {values['_STOP_LOSS_VALUE_']}", title = 'Confirm Sell!') == 'Yes':
                balance = get_balance(exchange=values['_EXCHANGE_'] ,currency=values['_TICKER_'].split("/")[0])
                #make sell order
                if values['_QTY_']:
                    amount = values['_AMOUNT_TO_SELL_']
                elif values['_PER_']:
                    amount = balance['free'] * float((values['_AMOUNT_TO_SELL_'] / 100))
                else:
                    pass
                print (amount)
                #make_sell_order(exchange=values['_EXCHANGE_'], symbol=values['_TICKER_'], amount=amount, price=values['_SELL_PRICE_'] )
            else:
                print('YAbta fil habta')
        
        #schedule.run_pending()
    window.close()

if __name__ == "__main__":
    main()
    
