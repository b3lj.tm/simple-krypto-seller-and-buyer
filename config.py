# Inspired By: @edeng23
import json
import os

# Config consts
CFG_FL_NAME = "config/config.json"


class Config:  # pylint: disable=too-few-public-methods,too-many-instance-attributes
    def __init__(self):
        # read configs
        if not os.path.exists(CFG_FL_NAME):
            print(f"No configuration file {CFG_FL_NAME} found! ")
        else:
            with open(CFG_FL_NAME) as f:
                data = json.load(f)
                # binance
                self.binance_api_key = data["binance"]["binance_api_key"]
                self.binance_secret_key = data["binance"]["binance_secret_key"]
                # kraken
                self.kraken_api_key = data["kraken"]["kraken_api_key"]
                self.kraken_secret_key = data["kraken"]["kraken_secret_key"]
                # supported_coins
                self.supported_coins = data["supported_coins"]
                # Check_balance_sleep_time
                self.SCHEDULER_SECs = data["Check_balance_sleep_time"]
                # supported_tickers
                self.supported_tickers = data["supported_tickers"]
