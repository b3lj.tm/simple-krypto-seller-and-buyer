#!python3
# Implemented By: @b3ljTM

import ccxt

from config import Config
from database import Database
from logger import Logger


class ExchangeAPIManager:
    def __init__(self, exchange: str, config: Config, db: Database, logger: Logger):
        # initializing the client class calls `ping` API endpoint, verifying the connection
        self.db = db
        self.logger = logger
        self.config = config
        self.exchange = exchange

        if "binance" == self.exchange:
            self.exchange_client = ccxt.binance(
                {'apiKey': self.config.binance_api_key, 'secret': self.config.binance_secret_key, 'enableRateLimit': True})
        elif "kraken" == self.exchange:
            self.exchange_client = ccxt.kraken(
                {'apiKey': self.config.kraken_api_key, 'secret': self.config.kraken_secret_key, 'enableRateLimit': True})
        elif "coinbase" == self.exchange:
            self.exchange_client = ccxt.kraken(
                {'apiKey': self.config.coinbase_api_key, 'secret': self.config.coinbase_secret_key, 'enableRateLimit': True})
        else:
            self.logger.error(f"{self.exchange} is not supported!")

    def get_fee(self, from_coin: str, to_coin: str, type: str, side: str, amount: float, price: float, takerOrMaker: str):
        try:
            self.exchange_client.calculate_fee(from_coin+"/"+to_coin, type, side, amount, price, takerOrMaker, {})
        except Exception as e:
            self.logger.error(f"error while getting fees for: {from_coin}/{to_coin} \n\t{e}" in {self.exchange})

    def get_orders(self):
        ret = []
        for ticker in self.config.supported_tickers:
            try:
                order = self.exchange_client.fetch_orders(symbol=ticker)
                if order:
                    ret.append(order)
            except Exception as e:
                self.logger.error(
                    f"error while fetching orders for {ticker} \n\t{e}" in {self.exchange})
        return ret

    def get_price(self, ticker):
        try: 
            price = self.exchange_client.fetch_ticker(ticker)['last']
            return price
        except Exception as e:
            self.logger.error(f"error while getting price for: {ticker} \n\t{e}" in {self.exchange})

    def get_order_status(self, order):
        try: 
            status = self.exchange_client.parse_order_status(order)
            return status
        except Exception as e:
            self.logger.error(f"error while getting the status for: {order} \n\t{e}" in {self.exchange})

    def set_buy_order(self, symbol, amount, price):
        try: 
            order = self.exchange_client.create_limit_sell_order(symbol, amount, price)
            return order
        except Exception as e:
            self.logger.error(f"error while making buy order for: {symbol} \n\t{e}" in {self.exchange})

    def set_sell_order(self, symbol, amount, price):
        try: 
            order = self.exchange_client.create_limit_buy_order(symbol, amount, price)
            return order
        except Exception as e:
            self.logger.error(f"error while making sell order for: {symbol} \n\t{e}" in {self.exchange})

    def cancel_order(self,id, symbol):
        try: 
            order = self.exchange_client.cancel_order(id, symbol)
            return order
        except Exception as e:
            self.logger.error(f"error while cancelling order for: {symbol} \n\t{e}" in {self.exchange})

    def get_balance(self,currency):
            try: 
                order = self.exchange_client.fetch_balance()
                return order[currency]
            except Exception as e:
                self.logger.error(f"error getting: {currency} balance \n\t{e}" in {self.exchange})
